## Hi and welcome to the Funding Circle code challenge ReadMe

I've build the project using VIPER, and RxSwift/RxCocoa. So it's RxViper :D

I've added a few bits and bobs just to give it life e.g. the Funding Circle icons for loading screen and nabber.

I've also given a compliment of unit tests for all screens. The unit tests are not exhaustive by any regard, but rather a sample, also to keep it short and sweet.

I've taken the app through instruments, and static analyser to make sure the code is mint. No issues have risen.

No **autolayout** issues :DDDD

I've used stack views religiously, I think their neat, but by no means does it reflect what can be done with auto layout either programmatically, VFL, etc

Well, I think that's all the info that's relevant. Hope you have fun critiquing my code >:(

Be gentle 8)

### Installation instructions

1. Clone
2. Run pod install
3. Tea/Coffee/<T: Drink>
4. Run

List of tools used:

JSON structs created using:
https://app.quicktype.io/

JSON beautify:
https://jsonformatter.org

![alt text](https://i.pinimg.com/originals/84/da/87/84da87272f7bf2cf4fc2db057e90d3e7.png)