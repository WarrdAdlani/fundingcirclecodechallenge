//
//  AuctionsRouter.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class AuctionsRouter: RouterProtocol, AuctionsRouterProtocol {
    var router: RouterProtocol?
    
    func createModule<T>(context: T? = nil, auctions: [Auction]? = nil) -> T? where T : View {
        // Declarations
        var view: AuctionsViewProtocol
        var presenter: AuctionsPresenterProtocol & AuctionsOutputInteractorProtocol
        var interactor: AuctionsInputInteractor
        var service: WebServiceProtocol
        var networkDispatcher: NetworkDispatcherProtocol
        
        // Instantiations
        view = instantiateViewController(identifier: "AuctionsViewController") as! AuctionsViewProtocol
        view.title = "Funding Circle"
        
        networkDispatcher = NetworkDispatcher()
        service = WebService(dispatcher: networkDispatcher)
        presenter = AuctionsPresenter()
        interactor = AuctionsInputInteractor(presenter: presenter, service: service)
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        presenter.router = self
        view.presenter = presenter

        return view as? T
    }
    
    func show(context view: View, with auction: Auction) {
        let auctionDetailsRouter = AuctionDetailsRouter()
        let auctionsDetailView = auctionDetailsRouter.createModule(context: view, auctions: [auction])
        view.navigationController?.pushViewController(auctionsDetailView!, animated: true)
    }
}
