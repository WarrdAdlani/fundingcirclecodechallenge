//
//  AuctionsInteractor.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AuctionsInputInteractor: AuctionsInputInteractorProtocol {
    weak var presenter: AuctionsOutputInteractorProtocol?
    var service: WebServiceProtocol?
    fileprivate var disposeBag: DisposeBag
    var interactorSubject: PublishSubject<Auctions>?
    
    init(presenter: AuctionsOutputInteractorProtocol, service: WebServiceProtocol?) {
        self.presenter = presenter
        self.service = service
        self.disposeBag = DisposeBag()
        self.interactorSubject = PublishSubject<Auctions>()
    }
    
    // Presenter to interactor subject
    func subscribeToSubject(_ subject: PublishSubject<Void>) {
        subject.subscribe(onNext: { [weak self] (event) in
            self?.getAuctions()
        }, onError: { [weak self] error in
            self?.presenter?.didGetError(error)
        }) {
            print("presenter disposed")
        }.disposed(by: disposeBag)
    }
        
    func getAuctions() {
        guard let service = service else {
            fatalError("service cannot be nil")
        }
        AuctionsWorker.getAuctions(service: service) { [weak self] (auctions, error) in
            if auctions != nil {
                guard let auctions = auctions as? Auctions else {
                    fatalError("failed to cast auctions")
                }
                self?.interactorSubject?.onNext(auctions)
            } else if let error = error {
                self?.interactorSubject?.onError(error)
            }
        }
    }
}
