//
//  AuctionsCellViewModel.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

class AuctionsCellViewModel {
    var model: Auction!
    var title: String {
        return model.title
    }
    var amount: String {
        return "Amount: ¢\(model.amountCents)"
    }
    var riskBand: String {
        return "Risk band: \(model.riskBand)"
    }
    var closeTime: String {
        let date = Date.dateFromString(model.closeTime, format: .yearMonthDay)
        let closeTime = date?.dateAsString(format: .dayMonthYearTimeLong)
        return "Close Time: " + closeTime!
    }
    var risk: RiskBand {
        guard let riskBand = RiskBand(rawValue: model.riskBand) else {
            fatalError("riskBand cannot be nil")
        }
        return riskBand
    }
    
    init(model: Auction) {
        self.model = model
    }
}
