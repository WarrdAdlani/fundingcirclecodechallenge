//
//  AuctionCell.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

protocol AuctionCellPresentable {
    func setupCell(model: AuctionsCellViewModel)
}

class AuctionCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var riskBandLabel: UILabel!
    
    @IBOutlet weak var closeTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

extension AuctionCell: AuctionCellPresentable {
    func setupCell(model: AuctionsCellViewModel) {
        titleLabel.text = model.title
        amountLabel.text = model.amount
        riskBandLabel.text = model.riskBand
        closeTimeLabel.text = model.closeTime
        
        let good = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        let reasonable = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let bad = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        
        switch model.risk {
        case .APlus:
            riskBandLabel.textColor = good
        case .A:
            riskBandLabel.textColor = good
        case .B:
            riskBandLabel.textColor = reasonable
        case .C:
            riskBandLabel.textColor = bad
        case .CMinus:
            riskBandLabel.textColor = bad
        }
    }
}
