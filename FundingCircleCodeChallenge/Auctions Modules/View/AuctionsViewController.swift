//
//  ViewController.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 08/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuctionsViewController: UIViewController, AuctionsViewProtocol {

    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Variables
    var refreshControl: UIRefreshControl!
    var presenter: AuctionsPresenterProtocol!
    
    // RXSwift initializations
    var viewSubject = PublishSubject<Void>()
    fileprivate var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logo = UIImage(named: "fsNavBarLogo.jpg")
        let imageView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 38, height: 38))
        imageView.image = logo
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        tableView.tableFooterView = UIView()
        refreshControl = UIRefreshControl(frame: CGRect.zero)
        refreshControl.addTarget(self, action: #selector(refresh), for: .allEvents)
        tableView.addSubview(refreshControl)
        
        // RXSwift assignments
        presenter.subscribeToViewSubject(viewSubject)
        viewSubject.onNext(())
        subscribeToVariable(presenter.presenterToViewSubject)
    }

    // Presenter to View subject
    func subscribeToVariable(_ subject: PublishSubject<Void>) {
        subject.subscribe(onNext: {
            self.didEndLoading()
        }, onError: { [unowned self] error in
            self.showAlert("Error", message: error.localizedDescription)
        }) {
            print("presenter disposed")
        }.disposed(by: disposeBag)
    }
    
    @objc func refresh() {
        self.didBeginLoading()
        presenter.refresh()
    }
    
    func showError(_ error: Error) {
        showAlert("Error", message: error.localizedDescription)
    }
    
    func showAlert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "okay", style: .default, handler: nil)
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - TableView DataSource
extension AuctionsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AuctionCell") as? AuctionCellPresentable else {
            return UITableViewCell()
        }
        
        let index = indexPath.row
        presenter.setupCell(cell, with: index)
        
        return cell as! UITableViewCell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = UINib.init(nibName: "AuctionsHeaderView", bundle: .main)
        let headerView = nib.instantiate(withOwner: self, options: nil).first
        return headerView as? UIView
    }
}

// MARK: - TableView Delegate
extension AuctionsViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        presenter.showAuction(at: index)
    }
}

// MARK: - Presenter delegate
extension AuctionsViewController {
    func didBeginLoading() {
        tableView.alpha = 0.4
        activityIndicator.startAnimating()
    }
    
    func didEndLoading() {
        tableView.reloadData()
        tableView.alpha = 1.0
        activityIndicator.stopAnimating()
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
}
