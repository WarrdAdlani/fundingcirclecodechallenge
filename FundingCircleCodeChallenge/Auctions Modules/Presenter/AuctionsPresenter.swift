//
//  AuctionsPresenter.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AuctionsPresenter: AuctionsPresenterProtocol {
    weak var view: AuctionsViewProtocol?
    var numberOfRows: Int {
        // Extract value from BehaviourSubject wrapper
        guard let result = auctions?.value else {
            return 0
        }
        return result.count
    }
    var numberOfSections: Int {
        return 1
    }
    var interactor: AuctionsInputInteractorProtocol? {
        didSet {
            guard let interactorSubject = interactor?.interactorSubject else {
                fatalError("interactorSubject is nil")
            }
            subscribeToInteractorSubject(interactorSubject)
        }
    }
    var router: AuctionsRouterProtocol?
    var auctions: BehaviorRelay<[Auction]>?
    var presenterToInteractorSubject: PublishSubject<Void>
    var presenterToViewSubject: PublishSubject<Void>
    fileprivate var disposeBag: DisposeBag
    
    init() {
        disposeBag = DisposeBag()
        auctions = BehaviorRelay(value: [])
        presenterToInteractorSubject = PublishSubject<Void>()
        presenterToViewSubject = PublishSubject<Void>()
        auctions?.subscribe(onNext: { [weak self] event in
            self?.presenterToViewSubject.onNext(())
        }) {
            print("auctions disposed")
        }.disposed(by: disposeBag)
    }
    
    func viewDidLoad() {
        getAuctions()
    }
    
    func refresh() {
        getAuctions()
    }
    
    fileprivate func getAuctions() {
        interactor?.subscribeToSubject(presenterToInteractorSubject)
        presenterToInteractorSubject.onNext(())
    }
    
    // View to presenter subject
    func subscribeToViewSubject(_ subject: PublishSubject<Void>) {
        subject.subscribe(onNext: { [weak self] in
            self?.viewDidLoad()
        }) {
            print("view disposed")
        }.disposed(by: disposeBag)
    }
    
    // Interactor to Presenter subject
    func subscribeToInteractorSubject(_ subject: PublishSubject<Auctions>) {
        subject.subscribe(onNext: { [weak self] auctions in
                self?.didGetAuctions(auctions.items)
            }, onError: { [weak self] error in
               self?.didGetError(error)
        }) {
            print("interactor disposed")
        }.disposed(by: disposeBag)
    }
    
    func setupCell(_ cell: AuctionCellPresentable, with index: Int) {
        guard let auctions = auctions?.value else {
            fatalError("auctions is nil")
        }
        let auction = auctions[index]
        let viewModel = AuctionsCellViewModel(model: auction)
        cell.setupCell(model: viewModel)
    }
    
    func showAuction(at index: Int) {
        guard let auctions = auctions?.value else {
            fatalError("auctions is nil")
        }
        let auction = auctions[index]
        router?.show(context: view as! View, with: auction)
    }
}

extension AuctionsPresenter: AuctionsOutputInteractorProtocol {
    func didGetAuctions(_ auctions: [Auction]?) {
        guard let auctions = auctions else {
            fatalError("auctions cannot be nil")
        }
        self.auctions?.accept(auctions)
    }
    
    func didGetError(_ error: Error) {
        self.presenterToViewSubject.onError(error)
    }
}
