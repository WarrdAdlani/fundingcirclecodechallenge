//
//  Auction.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: - Auction
struct Auctions: Codable {
    let items: [Auction]
    
    enum CodingKeys: String, CodingKey {
        case items = "items"
    }
}

// MARK: - Item
struct Auction: Codable {
    let id: Int
    let title: String
    let rate: Double
    let amountCents: Int
    let term: Int
    let riskBand: String
    let closeTime: String
    
    enum CodingKeys: String, CodingKey {
        case id, title, rate
        case amountCents = "amount_cents"
        case term
        case riskBand = "risk_band"
        case closeTime = "close_time"
    }
}
