//
//  AuctionsWorker.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

class AuctionsWorker {
    class func getAuctions(service: WebServiceProtocol, completion: @escaping NetworkResponseCompletion) {
        service.getAuctions(completion: completion)
    }
}
