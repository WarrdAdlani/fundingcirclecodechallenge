//
//  AuctionsProtocols.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: Presenter
protocol AuctionsRouterProtocol {
    func show(context: View, with auction: Auction)
}

// MARK: View
protocol AuctionsViewProtocol: class {
    var presenter: AuctionsPresenterProtocol! { get set }
    var title: String? { get set }
    var viewSubject: PublishSubject<Void> { get set }
}

// MARK: Presenter
protocol AuctionsPresenterProtocol: class {
    var view: AuctionsViewProtocol? { get set }
    var router: AuctionsRouterProtocol? { get set }
    var interactor: AuctionsInputInteractorProtocol? { get set }
    var numberOfRows: Int { get }
    var numberOfSections: Int { get }
    var auctions: BehaviorRelay<[Auction]>? { get set }
    var presenterToInteractorSubject: PublishSubject<Void> { get set }
    var presenterToViewSubject: PublishSubject<Void> { get set }
    func viewDidLoad()
    func setupCell(_ cell: AuctionCellPresentable, with index: Int)
    func showAuction(at index: Int)
    func subscribeToViewSubject(_ subject: PublishSubject<Void>)
    func refresh()
}

// MARK: Input Interactor
protocol AuctionsInputInteractorProtocol: class {
    var presenter: AuctionsOutputInteractorProtocol? { get set }
    var interactorSubject: PublishSubject<Auctions>? { get set }
    
    func subscribeToSubject(_ subject: PublishSubject<Void>)
    func getAuctions()
}

// MARK: Output interactor
protocol AuctionsOutputInteractorProtocol: class {
    func didGetAuctions(_ auctions: [Auction]?)
    func didGetError(_ error: Error)
}
