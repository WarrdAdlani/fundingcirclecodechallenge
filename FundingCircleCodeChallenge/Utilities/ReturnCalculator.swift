//
//  ReturnCalculator.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

enum RiskBand: String {
    case APlus = "A+"
    case A = "A"
    case B = "B"
    case C = "C"
    case CMinus = "C-"
}

public struct ReturnCalculator {
    static func calculateEstimatedReturnAmount(rate: Double, riskBand: String, fee: Double = 0.01, bindAmount: Int = 20) -> Double? {
        guard let risk: RiskBand = RiskBand(rawValue: riskBand) else {
            return nil
        }
        
        var ebd = 0.0
        
        switch risk {
        case .APlus:
            ebd = 0.01
        case .A:
            ebd = 0.02
        case .B:
            ebd = 0.03
        case .C:
            ebd = 0.04
        case .CMinus:
            ebd = 0.05
        }
        
        /* Formula used: era = (1 + ar − ebd − f) * ba
         * era is the estimated return amount in GBP.
         * ar is the auction's rate.
         * ebd is the estimated bad debt associated to the auction's risk band.
         * f is the fee, default is 0.01.
         * ba is the bid amount, default is £20.
         * =======================
         * Variable name conventions
         * rate = ar
         * ebd = ebd
         * fee = fee
         * bidAmount = ba
         */
        let era = (1 + rate - ebd - fee) * Double(bindAmount)
        return era.rounded(toPlaces: 2)
    }
    
    // Helper method
    static func calculateEstimatedReturnAmount(with auction: Auction, fee: Double = 0.01, bidAmount: Int = 20) -> Double? {
        
        let rate = auction.rate
        let riskBand = auction.riskBand
        guard let era = ReturnCalculator.calculateEstimatedReturnAmount(rate: rate, riskBand: riskBand, fee: fee.rounded(toPlaces: 2), bindAmount: bidAmount) else {
            return 0.0
        }
        
        return era
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
