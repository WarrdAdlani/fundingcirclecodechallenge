//
//  Date+Extension.swift
//  Spring
//
//  Created by Warrd Adlani on 09/05/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

extension Date {
    
    public static func dateFromString(_ string: String, format: Constants.DateFormats = .dayMonthYearLong) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        
        guard let date = dateFormatter.date(from: string) else {
            return nil
        }
        
        return date
    }
    
    public static func stringFromDate(_ date: Date, format: Constants.DateFormats = .dayMonthYearLong) -> String? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    public func dateAsString(format: Constants.DateFormats = .dayMonthYearLong) -> String? {
        return Date.stringFromDate(self, format: format)
    }
    
    static func getDateByAdding(days: Int, to date: Date, using calendar: Calendar = Calendar.current) -> Date? {
        return calendar.date(byAdding: .day, value: days, to: date)
    }
    
    func getDateBySubtracting(days: Int, using calendar: Calendar = Calendar.current) -> Date? {
        return calendar.date(byAdding: .day, value: -days, to: self)
    }
    
    func getDateByAdding(days: Int, using calendar: Calendar = Calendar.current) -> Date? {
        return calendar.date(byAdding: .day, value: days, to: self)
    }
}
