//
//  AuctionDetailsRouter.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class AuctionDetailsRouter: RouterProtocol {
    var router: RouterProtocol?
    
    func createModule<T>(context: T?, auctions: [Auction]?) -> T? where T : View {
        // Declarations
        var view: AuctionDetailsViewProtocol
        var presenter: AuctionDetailsPresenterProtocol & AuctionDetailsOutputInteractorProtocol
        var interactor: AuctionDetailsInputInteractorProtocol
        
        // Instantiations and assignments
        view = instantiateViewController(identifier: "AuctionDetailsViewController") as! AuctionDetailsViewProtocol
        view.title = "Auction Details"
        guard let auction = auctions?.first else {
            fatalError("auction cannot be nil")
        }
        presenter = AuctionDetailsPresenter(auction: auction)
        interactor = AuctionDetailsInteractor(presenter: presenter)
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        view.presenter = presenter
        
        return view as? T
    }
    
    deinit {
        #if DEBUG
        print("deinit")
        #endif
    }
}
