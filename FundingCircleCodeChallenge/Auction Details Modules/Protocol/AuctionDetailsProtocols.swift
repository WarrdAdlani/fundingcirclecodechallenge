//
//  AuctionDetailsProtocols.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: View
protocol AuctionDetailsViewProtocol: class {
    var presenter: AuctionDetailsPresenterProtocol! { get set }
    var title: String? { get set }
    
    func updateDetails()
}

// MARK: Presenter
protocol AuctionDetailsPresenterProtocol: class {
    var view: AuctionDetailsViewProtocol? { get set }
    var interactor: AuctionDetailsInputInteractorProtocol? { get set }
    var auction: Auction! { get set }
    var auctionName: String! { get }
    var amount: Int! { get }
    var riskBand: String! { get }
    var risk: RiskBand? { get }
    var closeTime: String! { get }
    var estimatedReturnAmount: Double! { get set }
    var fee: BehaviorRelay<Double> { get set }
    var bidAmount: BehaviorRelay<Int> { get set }
    var presenterToInteractorSubject: PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)> { get set }
    var presenterToViewSubject: PublishSubject<Void> { get set }
    
    func viewDidLoad()
    func subscribeToViewSubject(_ subject: PublishSubject<Void>) 
}

// MARK: Input Interactor
protocol AuctionDetailsInputInteractorProtocol: class {
    var presenter: AuctionDetailsOutputInteractorProtocol? { get set }
    var interactorToPresenterSubject: PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)> { get set }
    
    func getEstimatedReturnAmount(for auction: Auction, fee: Double, bidAmount: Int)
    func subscribeToSubject(_ subject: PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)>)
}

// MARK: Output interactor
protocol AuctionDetailsOutputInteractorProtocol: class {
    func didGetEstimatedReturnAmount(_ era: Double)
    func showAuction(_ auction: Auction)
}

