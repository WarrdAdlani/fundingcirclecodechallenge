//
//  AuctionDetailsInteractor.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AuctionDetailsInteractor: AuctionDetailsInputInteractorProtocol {
    weak var presenter: AuctionDetailsOutputInteractorProtocol?
    var interactorToPresenterSubject: PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)>
    fileprivate var disposeBag = DisposeBag()
    
    init(presenter: AuctionDetailsOutputInteractorProtocol) {
        self.presenter = presenter
        self.interactorToPresenterSubject = PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)>()
    }
    
    func subscribeToSubject(_ subject: PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)>) {
        subject.subscribe(onNext: { [weak self] (auction: Auction, fee: Double, bidAmount: Int) in
            self?.getEstimatedReturnAmount(for: auction, fee: fee, bidAmount: bidAmount)
        }) {
            #if DEBUG
            print("presenter disposed")
            #endif
        }.disposed(by: disposeBag)
    }
    
    func getEstimatedReturnAmount(for auction: Auction, fee: Double, bidAmount: Int) {
        let era = AuctionDetailsWorker.getEstimatedReturnAmount(for: auction, fee: fee, bidAmount: bidAmount)
        presenter?.didGetEstimatedReturnAmount(era!)
    }
    
    deinit {
        #if DEBUG
        print("deinit")
        #endif
    }
}
