//
//  AuctionDetailsViewController.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AuctionDetailsViewController: UIViewController, AuctionDetailsViewProtocol {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var riskBandLabel: UILabel!
    @IBOutlet weak var closeTimeLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var bidAmountLabel: UILabel!
    @IBOutlet weak var estimatedReturnLabel: UILabel!
    @IBOutlet weak var feeSlider: UISlider!
    @IBOutlet weak var bidSlider: UISlider!
    
    var presenter: AuctionDetailsPresenterProtocol!
    var viewSubject = PublishSubject<Void>()
    fileprivate let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.subscribeToViewSubject(viewSubject)
        presenter.presenterToViewSubject.subscribe(onNext: { [weak self] in
            self?.updateDetails()
        }) {
            print("presenter disposed")
        }.disposed(by: disposeBag)
        
        viewSubject.onNext(())
        
        feeSlider.addTarget(self, action: #selector(didMoveFeeSlider(_:)), for: .valueChanged)
        bidSlider.addTarget(self, action: #selector(didMoveBidSlider(_:)), for: .valueChanged)
    }
    
    func updateDetails() {
        titleLabel.text = presenter.auctionName
        amountLabel.text = "Amount ¢\(presenter.amount ?? 0)"
        riskBandLabel.text = "Risk Band: " + presenter.riskBand
        closeTimeLabel.text = "Close Time: " + presenter.closeTime
        estimatedReturnLabel.text = "Estimated Return Amount = \(presenter.estimatedReturnAmount ?? 0)"
        feeLabel.text = "Fee: \(presenter.fee.value)%"
        bidAmountLabel.text = "Bid Amount: £\(presenter.bidAmount.value)"
        
        let good = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        let reasonable = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        let bad = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        let riskBand: RiskBand = presenter.risk!
        
        switch riskBand {
        case .APlus:
            riskBandLabel.textColor = good
        case .A:
            riskBandLabel.textColor = good
        case .B:
            riskBandLabel.textColor = reasonable
        case .C:
            riskBandLabel.textColor = bad
        case .CMinus:
            riskBandLabel.textColor = bad
        }
    }
    
    deinit {
        print("deinit")
    }
}

extension AuctionDetailsViewController {
    @objc func didMoveFeeSlider(_ slider: UISlider) {
        presenter.fee.accept(Double(slider.value).rounded(toPlaces: 2))
    }
    
    @objc func didMoveBidSlider(_ slider: UISlider) {
        presenter.bidAmount.accept(Int(slider.value))
    }
}
