//
//  AuctionDetailsWorker.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 10/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

class AuctionDetailsWorker {
    class func getEstimatedReturnAmount(for auction: Auction, fee: Double, bidAmount: Int) -> Double? {
        return ReturnCalculator.calculateEstimatedReturnAmount(with: auction, fee: fee, bidAmount: bidAmount)
    }
    
    deinit {
        #if DEBUG
        print("deinit")
        #endif
    }
}
