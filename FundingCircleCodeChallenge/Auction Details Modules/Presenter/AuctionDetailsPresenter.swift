//
//  AuctionDetailsPresenter.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class AuctionDetailsPresenter: AuctionDetailsPresenterProtocol {
    weak var view: AuctionDetailsViewProtocol?
    var interactor: AuctionDetailsInputInteractorProtocol? {
        didSet {
            interactor?.subscribeToSubject(presenterToInteractorSubject)
        }
    }
    var auction: Auction!
    var auctionName: String! {
        return auction?.title
    }
    var amount: Int! {
        return auction?.amountCents
    }
    var riskBand: String! {
        return auction?.riskBand
    }
    var closeTime: String! {
        let date = Date.dateFromString(auction.closeTime, format: .yearMonthDay)
        let closeTime = date?.dateAsString(format: .dayMonthYearTimeLong)
        return "Close Time: " + closeTime!
    }
    var risk: RiskBand? {
        return RiskBand(rawValue: auction!.riskBand)
    }
    var estimatedReturnAmount: Double!
    var fee: BehaviorRelay<Double>
    var bidAmount: BehaviorRelay<Int>
    var presenterToInteractorSubject: PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)>
    var presenterToViewSubject: PublishSubject<Void>
    
    fileprivate let disposeBag = DisposeBag()
    
    init(auction: Auction?) {
        self.auction = auction
        fee = BehaviorRelay(value: 0.01)
        bidAmount = BehaviorRelay(value: 20)
        presenterToInteractorSubject = PublishSubject<(auction: Auction, fee: Double, bidAmount: Int)>()
        presenterToViewSubject = PublishSubject<Void>()
        
        fee.subscribe(onNext: { [weak self] newFee in
            if let weakSelf = self {
                weakSelf.presenterToInteractorSubject.onNext((auction: weakSelf.auction, fee: newFee, bidAmount: weakSelf.bidAmount.value))
            }
        }) {
            #if DEBUG
            print("fee disposed")
            #endif
        }.disposed(by: disposeBag)
        
        bidAmount.subscribe(onNext: { [weak self] newBidAmount in
            if let weakSelf = self {
                weakSelf.presenterToInteractorSubject.onNext((auction: weakSelf.auction, fee: weakSelf.fee.value, bidAmount: newBidAmount))
            }
        }) {
            #if DEBUG
            print("bidAmount disposed")
            #endif
        }.disposed(by: disposeBag)
    }
    
    func subscribeToViewSubject(_ subject: PublishSubject<Void>) {
        subject.subscribe( onNext: { [weak self] in
            self?.viewDidLoad()
        }) {
            #if DEBUG
            print("bidAmount disposed")
            #endif
        }.disposed(by: disposeBag)
    }
    
    func viewDidLoad() {
        presenterToInteractorSubject.onNext((auction: auction, fee: 0.01, bidAmount: 30))
    }
    
    deinit {
        #if DEBUG
        print("deinit")
        #endif
    }
}

extension AuctionDetailsPresenter: AuctionDetailsOutputInteractorProtocol {
    func showAuction(_ auction: Auction) {
        presenterToViewSubject.onNext(())
    }
    
    func didGetEstimatedReturnAmount(_ era: Double) {
        estimatedReturnAmount = era
        presenterToViewSubject.onNext(())
    }
}
