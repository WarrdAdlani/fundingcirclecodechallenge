//
//  RouterProtocol.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

protocol RouterDelegate {}

typealias View = UIViewController

protocol RouterProtocol {
    var router: RouterProtocol? { get set }
    func createModule<T: View>(context: T?, auctions: [Auction]?) -> T?
}

extension RouterProtocol {
    func instantiateViewController(identifier: String) -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: .main)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
}
