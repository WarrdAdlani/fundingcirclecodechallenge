//
//  AppDelegate.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 08/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let router = AuctionsRouter()
        let viewController = router.createModule()
        window = UIWindow.init()
        let navigationController = UINavigationController(rootViewController: viewController!)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }
}

