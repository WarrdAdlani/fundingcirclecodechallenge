//
//  EndPoints.swift
//  FundingCircleCodeChallenge
//
//  Created by Warrd Adlani on 09/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

enum EndPoints: String {
    case auctions = "http://fc-ios-test.herokuapp.com/auctions"
}
