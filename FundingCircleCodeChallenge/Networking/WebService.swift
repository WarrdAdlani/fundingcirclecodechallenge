//
//  NetworkManager.swift
//  Employees
//
//  Created by Warrd Adlani on 27/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

public typealias NetworkResponseCompletion = (Any?, Error?)->()

public protocol WebServiceProtocol {
    var dispatcher: NetworkDispatcherProtocol! { get set }
    
    func getAuctions(completion: @escaping NetworkResponseCompletion)
}

// MARK: NetworkManager
public struct WebService: WebServiceProtocol {
    public var dispatcher: NetworkDispatcherProtocol!
    
    public init(dispatcher: NetworkDispatcherProtocol = NetworkDispatcher()) {
        self.dispatcher = dispatcher
    }
    
    // MARK: Methods for requests
    public func getAuctions(completion: @escaping NetworkResponseCompletion) {
        let request = GetAuctions()
        request.execute(dispatcher: dispatcher, onSuccess: { auctions in
            completion(auctions, nil)
        }) { error in
            completion(nil, error)
        }
    }
}

// MARK: - Requests
struct GetAuctions: RequestTypeProtocol {
    typealias ResponseType = Auctions
    var data: Request {
        return Request(path: EndPoints.auctions.rawValue)
    }
}
