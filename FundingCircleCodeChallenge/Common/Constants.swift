//
//  Constants.swift
//  Spring
//
//  Created by Warrd Adlani on 09/05/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit

public struct Constants {
    public enum DateFormats: String {
        case dayMonthYearTimeLong = "EEEE, MMMM dd, yyyy - HH:mm"
        case dayMonthYearLong = "E, d MMM yyyy"
        case dayMonthYearShort = "dd/MM/yyyy"
        case dayMonthLong = "d MMM"
        case dayMonthShort = "dd/MM"
        case yearMonthDay = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
//    2019-08-07T21:52:32.138Z
}
