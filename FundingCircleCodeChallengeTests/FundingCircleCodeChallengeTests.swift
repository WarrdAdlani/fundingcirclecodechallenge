//
//  FundingCircleCodeChallengeTests.swift
//  FundingCircleCodeChallengeTests
//
//  Created by Warrd Adlani on 08/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
@testable import FundingCircleCodeChallenge

class FundingCircleCodeChallengeTests: XCTestCase {
    
    let jsonDecoder = JSONDecoder()
    var jsonStub: String!
    var auction: Auction!
    
    override func setUp() {
        jsonStub = "{\"id\": 20,"
        + "\"title\": \"Conroy, Wyman and Bruen\","
        + "\"rate\": 0.04,"
        + "\"amount_cents\": 5000000,"
        + "\"term\": 24,"
        + "\"risk_band\": \"C-\","
        + "\"close_time\": \"2019-07-11T14:02:59.136Z\"}"
        
        guard let jsonData = jsonStub.data(using: .utf8) else {
            fatalError("json encoding failed")
        }
        auction = try! jsonDecoder.decode(Auction.self, from: jsonData)
    }
    
    override func tearDown() {}
    
    func test_CalculateEstimatedReturnAmountGivenDefaultValuesAndReturnResult() {
        XCTAssertNotNil(auction)
        let rate = auction.rate
        let riskBand = auction.riskBand
        let era = ReturnCalculator.calculateEstimatedReturnAmount(rate: rate, riskBand: riskBand)
        XCTAssert(era == 19.6)
    }
    
    func test_CalculateEstimatedReturnAmountGivenCustomValuesAndReturnResult() {
        XCTAssertNotNil(auction)
        let rate = auction.rate
        let riskBand = auction.riskBand
        let fee = 0.08
        let bidAmount = 40
        let era = ReturnCalculator.calculateEstimatedReturnAmount(rate: rate, riskBand: riskBand, fee: fee, bindAmount: bidAmount)
        XCTAssert(era == 36.4)
    }
    
    
    func test_CalculateEstimatedReturnAmounAndReturnResulttWithHelperMethodWithGivenCustomValues() {
        let fee = 0.05
        let bidAmount = 63
        let era = ReturnCalculator.calculateEstimatedReturnAmount(with: auction, fee: fee, bidAmount: bidAmount)
        XCTAssertNotNil(era)
        XCTAssert(era == 59.22)
    }
    
    func test_CalculateEstimatedReturnAmounAndReturnResulttWithHelperMethodWithGivenDefaultValues() {
        XCTAssertNotNil(auction)
        let era = ReturnCalculator.calculateEstimatedReturnAmount(with: auction)
        XCTAssert(era == 19.6)
    }
}



