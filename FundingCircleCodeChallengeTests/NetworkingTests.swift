//
//  NetworkingTests.swift
//  NetworkingTests
//
//  Created by Warrd Adlani on 28/06/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
@testable import FundingCircleCodeChallenge

class NetworkingTests: XCTestCase {
    var stubResponse = "[{\"id\": \"1\",\"name\": \"Bob\"}]"

    func test_RequestSuccess() {
        let request = GetEmployees()
        let mockDispatcher = MockDispatcher(jsonStub: stubResponse)
        request.execute(dispatcher: mockDispatcher, onSuccess: { (employees: [StubEmployee]) in
            let employee = employees.first
            XCTAssert(employee?.id == "1")
            XCTAssert(employee?.name == "Bob")
        }) { (error : Error) in
            XCTFail()
        }
    }
    
    func test_RequestFailure() {
        let request = GetEmployees()
        let mockDispatcher = MockDispatcher(jsonStub: nil)
        request.execute(dispatcher: mockDispatcher, onSuccess: { (employees: [StubEmployee]) in
            XCTFail()
        }) { (error : Error) in
            let error = error as NSError
            
            XCTAssert(error.code == 101)
            XCTAssert(error.domain == "network failure")
        }
    }
    
    func test_NetworkManagerHasDispatcherSet() {
        let mockDispatcher = MockDispatcher(jsonStub: nil)
        let networkManager = WebService(dispatcher: mockDispatcher)
        XCTAssertTrue(networkManager.dispatcher as! MockDispatcher == mockDispatcher)
    }
    
    func test_NetworkManagerSetsDefaultDispatcher() {
        let networkManager = WebService()
        let dispatcher = networkManager.dispatcher
        XCTAssertNotNil(dispatcher)
    }
    
    func test_NetworkManagerRequestSuccess() {
        let mockDispatcher = MockDispatcher(jsonStub: stubResponse)
        let mockNetworkManager = MockWebService(dispatcher: mockDispatcher)
        mockNetworkManager.getEmployees { (employees, error) in
            if let employees = employees {
                let employee = employees.first
                XCTAssert(employee?.id == "1")
                XCTAssert(employee?.name == "Bob")
            } else {
                XCTFail()
            }
        }
    }
    
    func test_NetworkManagerRequestFailure() {
        let mockDispatcher = MockDispatcher(jsonStub: nil)
        let mockNetworkManager = MockWebService(dispatcher: mockDispatcher)
        mockNetworkManager.getEmployees { (employees, error) in
            if let _ = employees {
                XCTFail()
            } else {
                guard let error = error as NSError? else { fatalError() }
                
                XCTAssert(error.code == 101)
                XCTAssert(error.domain == "network failure")
            }
        }
    }
    
    func test_DispatcherReturnsParsesJSONAndReturnsModel() {
        let mockDispatcher = MockDispatcher(jsonStub: stubResponse)
        mockDispatcher.dispatch(request: Request(path: nil), onSuccess: { (data) in
            let employees = try? JSONDecoder().decode(GetEmployees.ResponseType.self, from: data)
            XCTAssertNotNil(employees)
            let employee = employees?.first
            XCTAssertTrue(employee?.id == "1")
            XCTAssertTrue(employee?.name == "Bob")
        }) { (error) in
            XCTFail()
        }
    }
}

struct MockDispatcher: NetworkDispatcherProtocol, Equatable {
    let jsonStub: String?

    func dispatch(request: RequestProtocol, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void) {
        if let jsonData = jsonStub?.data(using: .utf8, allowLossyConversion: false) {
            onSuccess(jsonData)
        } else {
            onError(NSError.init(domain: "network failure", code: 101, userInfo: nil))
        }
    }
}

struct StubEmployee: Codable {
    let id: String = "1"
    let name: String = "Bob"
}

struct StubEmployees: Codable {
    let employees: [StubEmployee]
}

// MARK: Requests
struct GetEmployees: RequestTypeProtocol {
    typealias ResponseType = [StubEmployee]
    var data: Request {
        return Request(path: nil)
    }
}

struct MockWebService: WebServiceProtocol {
    func getAuctions(completion: @escaping NetworkResponseCompletion) {}
    
    var dispatcher: NetworkDispatcherProtocol!
}

extension MockWebService {
    func getEmployees(completion: @escaping ([StubEmployee]?, Error?)->()) {
        let requestEmployees = GetEmployees()
        requestEmployees.execute(dispatcher: dispatcher, onSuccess: { (employees: [StubEmployee]) in
            completion(employees, nil)
        }) { (error: Error) in
            completion(nil, error)
        }
    }
}
