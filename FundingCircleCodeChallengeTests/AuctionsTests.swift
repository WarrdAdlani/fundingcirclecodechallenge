//
//  AuctionsTests.swift
//  FundingCircleCodeChallengeTests
//
//  Created by Warrd Adlani on 10/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import UIKit
import RxSwift
import RxCocoa

@testable import FundingCircleCodeChallenge

class AuctionsTests: XCTestCase {

    var router: AuctionsRouter!
    
    lazy var mockViewController: AuctionsViewProtocol = {
        return router.createModule() as! AuctionsViewProtocol
    }()
    
    lazy var view: AuctionsViewController = {
        return mockViewController as! AuctionsViewController
    }()
    
    lazy var mockPresenter: MockPresenter = {
        return MockPresenter()
    }()
    
    override func setUp() {
        router = AuctionsRouter()
    }

    override func tearDown() {
        router = nil
    }
    
    func test_RouterCreatesView() {
        class MockRouter: RouterProtocol {
            var router: RouterProtocol?
            func createModule<T>(context: T? = nil, auctions: [Auction]? = nil) -> T? where T : View {
                return instantiateViewController(identifier: "AuctionsViewController") as? T
            }
        }
        
        let mockRouter = MockRouter()
        let view = mockRouter.createModule()
        XCTAssertNotNil(view)
    }
    
    func test_AuctionsCreateModuleCreatesView() {
        let view = router.createModule()
        XCTAssertNotNil(view)
    }
    
    func test_AuctionsCreateModuleCreatesModules() {
        let view = router.createModule() as! AuctionsViewProtocol
        XCTAssertNotNil(view.presenter)
        XCTAssertNotNil(view.presenter.interactor)
        XCTAssertNotNil(view.presenter.router)
    }
    
    func test_PresenterReturnsNumberOfRowsAndNumberOfSectionsDefaultValue() {
        let presenter = AuctionsPresenter()
        XCTAssertTrue(presenter.numberOfRows == 0)
        XCTAssertTrue(presenter.numberOfSections == 1)
    }
    
    func test_ViewSubscribesViesSubject() {
        let view = router.createModule() as! AuctionsViewProtocol
        XCTAssertNotNil(view.viewSubject)
    }
    
    func test_PresenterViewDidLoadIsCalled() {
        mockViewController.presenter = mockPresenter
        let view = mockViewController as! AuctionsViewController
        view.loadView()
        view.viewDidLoad()
        XCTAssertTrue(mockPresenter.spyViewDidLoad)
    }
    
    func test_PresenterInitializedPresenterSubject() {
        view.presenter = mockPresenter
        view.loadView()
        view.viewDidLoad()
        XCTAssertNotNil(mockPresenter.presenterToViewSubject)
    }
    
    func test_PresenterRefreshIsCalled() {
        view.presenter = mockPresenter
        view.loadView()
        view.viewDidLoad()
        view.refresh()
        XCTAssertTrue(mockPresenter.spyRefreshCalled)
    }
}

class MockPresenter: AuctionsPresenter {
    var spyViewDidLoad = false
    var spyDidSetupCell = false
    var spyRefreshCalled = false
    
    override func viewDidLoad() {
        spyViewDidLoad = true
    }
    
    override func refresh() {
        spyRefreshCalled = true
    }
    
    override func setupCell(_ cell: AuctionCellPresentable, with index: Int) {
        spyDidSetupCell = true
    }
}
