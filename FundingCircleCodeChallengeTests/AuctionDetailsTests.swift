//
//  AuctionDetailsTests.swift
//  FundingCircleCodeChallengeTests
//
//  Created by Warrd Adlani on 10/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import UIKit
import RxSwift
import RxCocoa

@testable import FundingCircleCodeChallenge

class AuctionDetailsTests: XCTestCase {

    let jsonDecoder = JSONDecoder()
    var jsonStub: String!
    var mockAuction: Auction!
    var mockAuctions: [Auction]!
    var defaultFee = 0.01
    
    override func setUp() {
        jsonStub = "{\"id\": 20,"
            + "\"title\": \"Conroy, Wyman and Bruen\","
            + "\"rate\": 0.04,"
            + "\"amount_cents\": 5000000,"
            + "\"term\": 24,"
            + "\"risk_band\": \"C-\","
            + "\"close_time\": \"2019-07-11T14:02:59.136Z\"}"
        
        guard let jsonData = jsonStub.data(using: .utf8) else {
            fatalError("json encoding failed")
        }
        mockAuction = try! jsonDecoder.decode(Auction.self, from: jsonData)
        mockAuctions = [mockAuction]
    }

    override func tearDown() {}
    
    func test_RouterCreatesModuleCorrectly() {
        let view = AuctionDetailsRouter().createModule(context: nil, auctions: [mockAuction]) as! AuctionDetailsViewProtocol
        XCTAssertNotNil(view)
        XCTAssert(view.title == "Auction Details")
        XCTAssertNotNil(view.presenter)
    }
    
    func test_PresenterViewIsAssigned() {
        let view = AuctionDetailsRouter().createModule(context: nil, auctions: [mockAuction]) as! AuctionDetailsViewProtocol
        XCTAssertNotNil(view)
        XCTAssertNotNil(view.presenter.view)
    }
    
    func test_PresenterHasCorrectAuction() {
        let view = AuctionDetailsRouter().createModule(context: nil, auctions: [mockAuction]) as! AuctionDetailsViewProtocol
        let presenter = view.presenter
        XCTAssertNotNil(presenter?.auction)
        XCTAssertTrue(presenter?.auction.id == 20)
        XCTAssertTrue(presenter?.auction.riskBand == "C-")
    }
    
    func test_EnumCorrectlySetsRiskBand() {
        let view = AuctionDetailsRouter().createModule(context: nil, auctions: [mockAuction]) as! AuctionDetailsViewProtocol
        let presenter = view.presenter
        let auction = presenter!.auction!
        let riskBand = RiskBand(rawValue: auction.riskBand)
        XCTAssertTrue(riskBand == .CMinus)
    }
    
    func test_PresenterPropertiesAreCorrectlySet() {
        let view = AuctionDetailsRouter().createModule(context: nil, auctions: [mockAuction]) as! AuctionDetailsViewProtocol
        let presenter = view.presenter!
        
        XCTAssertTrue(presenter.auctionName == "Conroy, Wyman and Bruen")
        XCTAssertTrue(presenter.amount == 5000000)
        XCTAssertTrue(presenter.risk == .CMinus)
        XCTAssertTrue(presenter.closeTime == "Close Time: Thursday, July 11, 2019 - 14:02")
        XCTAssertTrue(presenter.fee.value == 0.01)
        XCTAssertTrue(presenter.bidAmount.value == 20)
        
        XCTAssertNotNil(presenter.interactor)
        XCTAssertNotNil(presenter.presenterToViewSubject)
        XCTAssertNotNil(presenter.presenterToInteractorSubject)
    }
    
    func test_PresenterViewDidLoadIsCalled() {
        let viewDidLoadExpectation = XCTestExpectation(description: "viewDidLoad expectation")
        let view = MockAuctionDetailsRouter().createModule(context: nil, auctions: mockAuctions)
        let presenter = (view as! AuctionDetailsViewProtocol).presenter as! MockAuctionDetailsPresenter
        presenter.spyViewDidLoad = {
            XCTAssertTrue(true)
            viewDidLoadExpectation.fulfill()
        }
        view?.loadView()
        view?.viewDidLoad()
        
        wait(for: [viewDidLoadExpectation], timeout: 5.0)
    }
    
    func test_PresenterEstimatedReturnAmountVariableUpdatedGivenDefaultFeeAndBidAmount(){
        let eraExpectation = XCTestExpectation(description: "era expectation")
        let view = MockAuctionDetailsRouter().createModule(context: nil, auctions: mockAuctions)
        let presenter = (view as! AuctionDetailsViewProtocol).presenter as! MockAuctionDetailsPresenter
        let interactor = presenter.interactor as! MockAuctionDetailsInteractor
        interactor.spyGetEstimateCalled = { success in
            XCTAssertTrue(success)
            XCTAssertTrue(presenter.estimatedReturnAmount == 29.4)
            eraExpectation.fulfill()
        }
        view?.loadView()
        view?.viewDidLoad()

        wait(for: [eraExpectation], timeout: 5.0)
    }
    
    func test_PresemterEstimatedReturnAmountVariableUpdatedGivenCustomFeeAndBidAmount(){
        let eraExpectation = XCTestExpectation(description: "era expectation")
        let view = MockAuctionDetailsRouter().createModule(context: nil, auctions: mockAuctions)
        let presenter = (view as! AuctionDetailsViewProtocol).presenter as! MockAuctionDetailsPresenter
        let interactor = presenter.interactor as! MockAuctionDetailsInteractor
        
        // Allow the default values to be used upon viewDidLoad, then try other values i.e. usecase
        DispatchQueue.main.asyncAfter(deadline: DispatchTime(uptimeNanoseconds: 10000)) {
            interactor.spyGetEstimateCalled = { success in
                XCTAssertTrue(success)
                XCTAssertTrue(presenter.estimatedReturnAmount == 193.8)
                eraExpectation.fulfill()
            }
            presenter.presenterToInteractorSubject.onNext((auction: self.mockAuction, fee: 0.42, bidAmount: 340))
        }
        
        view?.loadView()
        view?.viewDidLoad()
        
        wait(for: [eraExpectation], timeout: 10.0)
    }
    
    func test_InteractorGetEstimatedReturnAmountCalled() {
        let eraExpectation = XCTestExpectation(description: "era expectation")
        let view = MockAuctionDetailsRouter().createModule(context: nil, auctions: mockAuctions)
        let presenter = (view as! AuctionDetailsViewProtocol).presenter
        let interactor = presenter?.interactor as! MockAuctionDetailsInteractor
        interactor.spyGetEstimateCalled = { success in
            XCTAssertTrue(success)
            eraExpectation.fulfill()
        }
        view?.loadView()
        view?.viewDidLoad()
        
        wait(for: [eraExpectation], timeout: 5.0)
    }
}

class MockAuctionDetailsRouter: AuctionDetailsRouter {
    override func createModule<T>(context: T?, auctions: [Auction]?) -> T? where T : View {
        // Declarations
        var view: AuctionDetailsViewProtocol
        var presenter: AuctionDetailsPresenterProtocol & AuctionDetailsOutputInteractorProtocol
        var interactor: AuctionDetailsInputInteractorProtocol
        
        // Instantiations and assignments
        view = instantiateViewController(identifier: "AuctionDetailsViewController") as! AuctionDetailsViewProtocol
        view.title = "Auction Details"
        guard let auction = auctions?.first else {
            fatalError("auction cannot be nil")
        }
        presenter = MockAuctionDetailsPresenter(auction: auction)
        interactor = MockAuctionDetailsInteractor(presenter: presenter)
        interactor.presenter = presenter
        presenter.interactor = interactor
        presenter.view = view
        view.presenter = presenter
        
        return view as? T
    }
}

class MockAuctionDetailsInteractor: AuctionDetailsInteractor {
    var spyGetEstimateCalled: ((Bool)->())?
    override func getEstimatedReturnAmount(for auction: Auction, fee: Double, bidAmount: Int) {
        super.getEstimatedReturnAmount(for: auction, fee: fee, bidAmount: bidAmount)
        spyGetEstimateCalled?(true)
    }
}

class MockAuctionDetailsPresenter: AuctionDetailsPresenter {
    var spyViewDidLoad: (()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        spyViewDidLoad?()
    }
}
